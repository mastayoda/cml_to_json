var fs = require('fs');
var os = require("os");
var r = require('rethinkdb');
var prettyjson = require('prettyjson');

var processed = 0;


function saveToDB(reaction){

	return new Promise(function (resolve, reject) {

		var productList = reaction.productList;
		var products = reaction.products;
		delete reaction.productList;
		delete reaction.products;
		var reactantList = reaction.reactantList;
		var reactants = reaction.reactants;
		delete reaction.reactantList;
		delete reaction.reactants;
		var spectatorList = reaction.spectatorList;
		var spectators = reaction.spectators;
		delete reaction.spectatorList;
		delete reaction.spectators;

		/* Insert product into table */
	    r.db(db).table("product").insert(productList)
	    .run(conn, function(err, productsRes) {

	        if(err){
	            console.log(err);
	            return;
	        }else{
	        /* Check if a fatal error ocurred */
	           // console.log("\nProducts Inserted...");
	        }

	    });/* End products insert */

		/* Insert reactant into table */
	    r.db(db).table("reactant").insert(reactantList)
	    .run(conn, function(err, reactantsRes) {

	        if(err){
	            console.log(err);
	            return;
	        }else{
	        /* Check if a fatal error ocurred */
	           // console.log("Reactants Inserted...");
	        }

	    });/* End reactant insert */

	    /* Insert reactant into table */
	    r.db(db).table("spectator").insert(spectatorList)
	    .run(conn, function(err, spectatorsRes) {

	        if(err){
	            console.log(err);
	            return;
	        }else{

	        /* Check if a fatal error ocurred */
	            //console.log("Spectators Inserted...");
	        }
	    });/* End reactant insert */

		/* Insert reaction into table */
	    r.db(db).table("reaction").insert(reaction)
	    .run(conn, function(err, reactionRes) {

	        if(err){
	            console.log(err);
	            return;
	        }else{

	        	var key = reactionRes.generated_keys[0];
	        	/* Check if a fatal error ocurred */
	            console.log((++processed) + " - Reaction Inserted...");
	            /* Setting all products-reaction keys */
	            products.forEach(function(p){
	            	p.reaction_id = key;
	            });
	           	/* Setting all products-reaction keys */
	            reactants.forEach(function(r){
	            	r.reaction_id = key;
	            });
	            /* Setting all products-reaction keys */
	            spectators.forEach(function(s){
	            	s.reaction_id = key;
	            });

	            /* Inserting products-reaction records */
			    r.db(db).table("reaction_product").insert(products)
			    .run(conn, function(err, res) {
			        if(err){
			            console.log(err);
			            return;
			        }else{
			            //console.log("reaction_product Inserted...");
			        }
			    });
	            /* Inserting products-reaction records */
			    r.db(db).table("reaction_reactant").insert(reactants)
			    .run(conn, function(err, res) {
			        if(err){
			            console.log(err);
			            return;
			        }else{
			            //console.log("reaction_reactant Inserted...");
			        }
			    });
	            /* Inserting products-reaction records */
			    r.db(db).table("reaction_spectator").insert(spectators)
			    .run(conn, function(err, res) {
			        if(err){
			            console.log(err);
			            return;
			        }else{
			            //console.log("reaction_spectator Inserted...");
			        }
			    });
	        }
	        /* Returning promise */
	        resolve();
	    });/* End reaction insert */
	});/* End promise */
}

/* XML TO JSON FUNCTION */
function parse(files){

	return new Promise(function (resolve, reject) {
		
		var fs = require('fs'),
			prettyjson = require('prettyjson'),
			validate = require('jsonschema').validate,
		    xml2js = require('xml2js');

		var errorCount = 0;
		var totalCount = 0;
		var logStream = fs.createWriteStream('errors.txt', {'flags': 'a'});
		var parser = new xml2js.Parser();
		parser.setMaxListeners(0);

		function readFile(f){
				fs.readFile(f, function(err, data) {
					try{
						
						/* If data is valid */
						if(!data){
							throw "fs.readFile data is invalid";
						}
						/* parse file */
					    parser.parseString(data, function (err, result) {
					    	try{
						    	/* reasigning resulting reaction */
						    	result = result.reaction;

						    	var reaction = {};
						    	/* Parsing reaction info */
						    	reaction.reactionSmiles = result["dl:reactionSmiles"][0];
						    	/* Instantiate product array */
						    	reaction.productList = [];
						    	reaction.products = [];

						    	if(result["productList"].length>1){
						    		logStream.write("\nfile: " + f + " has " +result["productList"].length +" products\n\n");
						    	}

						    	/* parsing product */
						    	result["productList"][0]["product"].forEach(function(p){

						    		/* new json product */
									var prod = {};

									var role;
									if(p["$"])
										role = p["$"]["role"];
									
									if(!role)
										role = "unknown";

									if(p["molecule"])
										prod.name = p["molecule"][0]["name"][0]["_"].toLowerCase();

									if(p["dl:entityType"])
										prod.entityType = p["dl:entityType"][0];

									var state;
									if(p["dl:state"])
										state = p["dl:state"][0];
									else
										state = "unknown";

									/* Amounts array */
									var amounts = [];
									
									/* Getting all amounts */
									if(p["amount"]){
										p["amount"].forEach(function(a){
											var amount = {};
											amount.unit = a["$"]["units"].split(":")[1];
											amount.value = (isNaN(a["_"]))?a["_"]:parseFloat(a["_"]);
											/* Adding amount to array */
											amounts.push(amount);
										});
									}

									/* Adding identifier */
									if(p["identifier"]){
										prod[p["identifier"][0]["$"]["dictRef"].split(":")[1]] = p["identifier"][0]["$"]["value"];
										
										if(p["identifier"][1]){
											prod[p["identifier"][1]["$"]["dictRef"].split(":")[1]] = p["identifier"][1]["$"]["value"];
										}
										else{
											prod[(prod.smiles)?"inchi":"smiles"] = "unknown";
										}
									}else{
										prod.smiles = "unknown";
										prod.inchi = "unknown";
									}

									/* Setting id */
									if(prod.inchi != "unknown")
									{
										prod.id = prod.inchi;
										prod.id_field = "inchi";
									}else if(prod.smiles != "unknown"){
										prod.id = prod.smiles;
										prod.id_field = "smiles";
									}else{
										prod.id = prod.name;
										prod.id_field = "name";
									}


						    		 /* Adding product  to list */
						    		 reaction.productList.push(prod);
						    		 reaction.products.push({product_id:prod.id, amounts: amounts, state:state, role:role});
						    	});

								/* reactions array */
								reaction.reactantList = [];
								reaction.reactants = [];

								/* Getting all reactancts */
								result["reactantList"][0]["reactant"].forEach(function(r){
									var reactant = {};
									
									var role;
									if(r["$"]["role"])
										role = r["$"]["role"];

									if(!role)
										role = "unknown";

									if(r["$"]["count"])
										reactant.count = (isNaN(r["$"]["count"]))?r["$"]["count"]:parseFloat(r["$"]["count"]);
									else
										reactant.count = 1;

									reactant.name = r["molecule"][0]["name"][0]["_"];

									/* Amounts array */
									var amounts = [];
									
									/* Getting all amounts */
									if(r["amount"]){
										r["amount"].forEach(function(a){

											var amount = {};
											amount.unit = a["$"]["units"].split(":")[1];
											amount.value = (isNaN(a["_"]))?a["_"]:parseFloat(a["_"]);
											/* Adding amount to array */
											amounts.push(amount);
										});
									}

									/* Adding identifier */
									if(r["identifier"]){
										reactant[r["identifier"][0]["$"]["dictRef"].split(":")[1]] = r["identifier"][0]["$"]["value"];
										if(r["identifier"][1]){
											reactant[r["identifier"][1]["$"]["dictRef"].split(":")[1]] = r["identifier"][1]["$"]["value"];
										}else{
											reactant[(reactant.smiles)?"inchi":"smiles"] = "unknown";
										}
									
									}else{
										reactant.smiles = "unknown";
										reactant.inchi = "unknown";
									}

									/* Setting id */
									if(reactant.inchi != "unknown")
									{
										reactant.id = reactant.inchi;
										reactant.id_field = "inchi";
									}else if(reactant.smiles != "unknown"){
										reactant.id = reactant.smiles;
										reactant.id_field = "smiles";
									}else{
										reactant.id = reactant.name;
										reactant.id_field = "name";
									}

									/* Adding reactant to array */
									reaction.reactantList.push(reactant);
									reaction.reactants.push({reactant_id:reactant.id, amounts: amounts, role: role});
								});

								/* reactions array */
								reaction.spectatorList = [];
								reaction.spectators = [];

								/* Getting all reactancts */
								if(result["spectatorList"][0]["spectator"]){
									result["spectatorList"][0]["spectator"].forEach(function(s){
										var spectator = {};
										
										var role;
										if(s["$"])
											spectator.role = s["$"]["role"];

										if(!role)
											role = "unknown";

										if(s["molecule"])
											spectator.name = s["molecule"][0]["name"][0]["_"];

										/* Amounts array */
										var amounts = [];
										
										/* Getting all amounts */
										if(s["amount"]){
											s["amount"].forEach(function(a){
												var amount = {};
												amount.unit = a["$"]["units"].split(":")[1];
												amount.value = (isNaN(a["_"]))?a["_"]:parseFloat(a["_"]);
												/* Adding amount to array */
												amounts.push(amount);
											});
										}

										/* Adding identifier */
										if(s["identifier"]){
											spectator[s["identifier"][0]["$"]["dictRef"].split(":")[1]] = s["identifier"][0]["$"]["value"];
											if(s["identifier"][1]){
												spectator[s["identifier"][1]["$"]["dictRef"].split(":")[1]] = s["identifier"][1]["$"]["value"];
											}else{
												spectator[(spectator.smiles)?"inchi":"smiles"] = "unknown";
											}
										}else{
											spectator.smiles = "unknown";
											spectator.inchi = "unknown";
										}

										/* Setting id */
										if(spectator.inchi != "unknown")
										{
											spectator.id = spectator.inchi;
											spectator.id_field = "inchi";
										}else if(spectator.smiles != "unknown"){
											spectator.id = spectator.smiles;
											spectator.id_field = "smiles";
										}else{
											spectator.id = spectator.name;
											spectator.id_field = "name";
										}

										/* Adding reactant to array */
										reaction.spectatorList.push(spectator);
										reaction.spectators.push({spectator_id:spectator.id, amounts: amounts, role:role});
									});
								}

								/* Incrementing processed count */
								totalCount++;

								//console.log(JSON.stringify(reaction));
								/* Insert data into DB, then recurse*/
								saveToDB(reaction).then(function(){

									/* start the recursion */
									if(files.length>0)
										readFile(files.shift());
									else
										resolve({processed: totalCount - errorCount, errors: errorCount});

								});
							}catch(error){

					    		totalCount++;
					    		errorCount++;
					    		logStream.write("\nfile: " + f + "\n");
					    		logStream.write(prettyjson.render(error)+ "\n\n");

								/* start the recursion */
								if(files.length>0)
									readFile(files.shift());
								else
									resolve({processed: totalCount - errorCount, errors: errorCount});
					    	}
					    }); /* End XML parsing */

					}catch(error){

			    		totalCount++;
			    		errorCount++;
			    		logStream.write("\nfile: " + f + "\n");
			    		logStream.write(prettyjson.render(error)+ "\n\n");

						/* start the recursion */
						if(files.length>0)
							readFile(files.shift());
						else
							resolve({processed: totalCount - errorCount, errors: errorCount});
			    	}
				}); /* End File Read */
		} /* End readFile function */
		/* start the recursion */
		readFile(files.shift());

	}); /* End promise */
	
	/* Closing error file */
	//logStream.end("");
}

var conn;
var db = "reactions";

/* Setting up database connection */
r.connect( {host: 'localhost', port: 28016}, function(err, connection) {
    /* if error, throw exception */
    if (err) throw err;
    /* Set the connection */
    conn = connection;
    /* logging connection message */
    console.log("Connection to database successful");
    start();
})


function start(){
	/* base path for reaction directories */
	var grantsPath = "/homes/vsantosu/scratch/chem_dataset/grants/";
	var applicationsPath = "/homes/vsantosu/scratch/chem_dataset/applications/";

	/* reading all reaction files paths */
	var grantsReactionFiles = fs.readFileSync('./cml_datasets_meta/grants_fileIndex.txt').toString().split("\n");
	var applicationsReactionFiles = fs.readFileSync('./cml_datasets_meta/applications_fileIndex.txt').toString().split("\n");

	/* Loggin size */
	console.log("Grants Reactions: " + grantsReactionFiles.length);
	console.log("Applications Reactions: " + applicationsReactionFiles.length);

	/* appending full paths */
	for(var i in grantsReactionFiles)
		grantsReactionFiles[i] = grantsPath + grantsReactionFiles[i];
	for(var i in applicationsReactionFiles)
		applicationsReactionFiles[i] = applicationsPath + applicationsReactionFiles[i];

	/* concatenating arrays arrays */
	var reactionFiles = grantsReactionFiles.concat(applicationsReactionFiles);
	console.log("Total Reactions: " + reactionFiles.length);

	/* Execute in parallel */
	var i,j,count = 0, total =0, batches = [],chunk = reactionFiles.length/(os.cpus().length);
	/* logging chunk size */
	console.log("Each chunk will be of "+ chunk.toFixed(0) + " elements.");
	/* Chunkify file array */
	for (i=0,j=reactionFiles.length; i<j; i+=chunk) {
	    /* Slicing Chunk */
	    batches.push(reactionFiles.slice(i,i+chunk));
	    /* Increasing count */
	    count++;
	}

	function processBatch(arr){
	    
		 parse(arr)
		.then(function (result) {
		  /* logging counters */
		  console.log(result); 
		  /* Total processed */
		  total += result.processed + result.errors;
		  /* terminate if all workers are done */
		  if(total == reactionFiles.length){
		  	 console.log("Done parsing " + total + " reactions!");
		  }else{
		  	console.log((--count) + " remaining...");
		  	/* Process next batch */
		  	setTimeout(function(){
		  		processBatch(batches.shift());
		  	},0);
		  }

		});
	}

	console.log((--count) + " remaining...");
	/* Start processing */
	processBatch(batches.shift());
}
